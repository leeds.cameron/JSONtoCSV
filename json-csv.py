#!/usr/bin/python
import sys, getopt 
import csv
import json

#Get command line args 
def main(argv):
	input_file = ''
	output_file = ''
	format = ''
	try:
			opts, args = getopt.getopt(argv, "hi:o:f:",["ifile=","ofile=","format="])
	except getopt.GetoptError:
		print ('json-csv.py -i <path to inputfile> -o <path to outputfile> -f <dump/pretty>')
	for opt, arg in opts:
		if opt == '-h':
			print ('json-csv.py -i <path to inputfile> -o <path to outputfile> -f <dump/pretty>')
			sys.exit(2)
		elif opt in ("-i", "--ifile"):
			input_file = arg
		elif opt in ("-o", "--format"):
			output_file = arg
		elif opt in ("-f", "--format"):
			format	= arg
	read_json(input_file, output_file, format)

#read csv file
def read_json(file, csv_file, format):
	json_rows = []
	with open(file) as jsonfile:
		reader = json.DictReader(csvfile)
		title = reader.fieldnames
		for row in reader: 
			json_rows.extend([{title[i]:row[title[i]] for i in range(len(title))}])
		write_csv(csv_file, json_rows, format)

#convert csv data into json and write it
def write_csv(data, csv_file, format):
	with open(csv_rows, "w") as f:
		if format == "pretty":
			f.write(csv.dumps(data, sort_keys=False, indent = 4, seperators=(',',': '),encoding="utf-8",ensure_ascii=False))
		else: 
			f.write(csv.dumps(data))
if __name__ == "_main_":
	main(sys.argv[1:])